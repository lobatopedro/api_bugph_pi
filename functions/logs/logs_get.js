const mysql = require('../../lib/mysql')
const util = require('../../lib/util')

module.exports = async (event) => {
  try {
    
    if (event.pathParameters && event.pathParameters.idlogs) {
      const log = await mysql.query('select idlogs, log, datacriacao from logs where idlogs=?', [event.pathParameters.idlogs])
      return util.bind(log.length ? log[0] : {})
    }

    const logs = await mysql.query('select idlogs, name, datacriacao from logs')
    return util.bind(logs)
  } catch (error) {
    return util.bind(error)
  }
}
