const mysql = require('../../lib/mysql')
const util = require('../../lib/util')

module.exports = async (event) => {
  try {
    const body = JSON.parse(event.body || '{}')
    if (!body.log) return util.bind(new Error('Enter log'))
    if (!body.id) return util.bind(new Error('Enter log id'))

    const checkLogExists = await mysql.query('select * from logs where id=?', [body.id])
    if (checkLogExists.length) return util.bind(new Error('Log already exists.'))

    const insert = await mysql.query('insert into logs (log, id) values (?,?)', [body.log, body.id])
    return util.bind(insert)
  } catch (error) {
    return util.bind(error)
  }
}
