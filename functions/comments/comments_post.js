const mysql = require('../../lib/mysql')
const util = require('../../lib/util')

module.exports = async (event) => {
  try {
    const body = JSON.parse(event.body || '{}')
    if (!body.comment) return util.bind(new Error('Enter comment'))
    //sequence generator

    const checkCommentExists = await mysql.query('select * from comments where id=?', [body.id])
    if (checkCommentExists.length) return util.bind(new Error('Already commented.'))

    const insert = await mysql.query('insert into comments (comment, id) values (?,?)', [body.comment, body.id])
    return util.bind(insert)
  } catch (error) {
    return util.bind(error)
  }
}
