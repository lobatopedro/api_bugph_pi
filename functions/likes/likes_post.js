const mysql = require('../../lib/mysql')
const util = require('../../lib/util')

module.exports = async (event) => {
  try {
    const request = JSON.parse(event.request || '{}')

    const checkLikeExist = await mysql.query('select * from likes where id=?', [request.id])
    if (checkLikeExist.length) return util.bind(new Error('Already liked.'))

    const insert = await mysql.query('insert into likes (datacriacao, id) values (?,?)', [request.datacriacao, request.id])
    return util.bind(insert)
  } catch (error) {
    return util.bind(error)
  }
}
