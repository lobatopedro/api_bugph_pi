const mysql = require('../../lib/mysql')
const util = require('../../lib/util')

module.exports = async (event) => {
  try {
    
    if (event.pathParameters && event.pathParameters.id) {
      const likes = await mysql.query('select id, datacriacao from likes where id=?', [event.pathParameters.id])
      return util.bind(likes.length ? likes[0] : {})
    }

    const likes = await mysql.query('select id, datacriacao from likes')
    return util.bind(likes)
  } catch (error) {
    return util.bind(error)
  }
}
