const get = require('./likes_get.js')
const post = require('./likes_post.js')

module.exports = {
  get,
  post,
  remove
}