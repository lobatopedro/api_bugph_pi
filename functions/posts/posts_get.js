const mysql = require('../../lib/mysql')
const util = require('../../lib/util')

module.exports = async (event) => {
  try {
    
    if (event.pathParameters && event.pathParameters.id) {
      const post = await mysql.query('select id, titulo, datacriacao from posts where id=?', [event.pathParameters.id])
      return util.bind(post.length ? post[0] : {})
    }

    const posts = await mysql.query('select id, titulo, datacriacao from posts')
    return util.bind(posts)
  } catch (error) {
    return util.bind(error)
  }
}
