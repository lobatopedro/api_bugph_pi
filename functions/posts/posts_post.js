const mysql = require('../../lib/mysql')
const util = require('../../lib/util')

module.exports = async (event) => {
  try {
    const body = JSON.parse(event.body || '{}')
    if (!body.title) return util.bind(new Error('Enter title'))
    if (!body.image) return util.bind(new Error('Enter your image'))

    const checkPostExists = await mysql.query('select * from posts where id=?', [body.id])
    if (checkPostExists.length) return util.bind(new Error('A post with this id already exists.'))

    const insert = await mysql.query('insert into posts (title, image) values (?,?)', [body.title, body.image])
    return util.bind(insert)
  } catch (error) {
    return util.bind(error)
  }
}
