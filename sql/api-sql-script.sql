
SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
-- -----------------------------------------------------
-- Table `users`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `users` (
  `id` INT NOT NULL COMMENT '',
  `name` VARCHAR(45) NOT NULL COMMENT '',
  `email` VARCHAR(45) NOT NULL COMMENT '',
  `senha` VARCHAR(45) NOT NULL COMMENT '',
  `ultimologin` DATE NOT NULL COMMENT '',
  `datacriacao` DATE NOT NULL COMMENT '',
  PRIMARY KEY (`id`)  COMMENT '')
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `posts`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `posts` (
  `id` INT NOT NULL COMMENT '',
  `titulo` VARCHAR(45) NOT NULL COMMENT '',
  `imagem` BLOB NULL COMMENT '',
  `datacriacao` DATE NOT NULL COMMENT '',
  `usuarios_idusuarios` INT NOT NULL COMMENT '',
  PRIMARY KEY (`id`)  COMMENT '',
  INDEX `fk_posts_usuarios_idx` (`usuarios_idusuarios` ASC)  COMMENT '',
  CONSTRAINT `fk_posts_usuarios`
    FOREIGN KEY (`usuarios_idusuarios`)
    REFERENCES `users` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `comments`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `comments` (
  `id` INT NOT NULL COMMENT '',
  `comentarios` VARCHAR(45) NOT NULL COMMENT '',
  `datacriacao` DATE NOT NULL COMMENT '',
  `posts_idposts` INT NOT NULL COMMENT '',
  `usuarios_idusuarios` INT NOT NULL COMMENT '',
  PRIMARY KEY (`id`)  COMMENT '',
  INDEX `fk_comentarios_posts1_idx` (`posts_idposts` ASC)  COMMENT '',
  INDEX `fk_comentarios_usuarios1_idx` (`usuarios_idusuarios` ASC)  COMMENT '',
  CONSTRAINT `fk_comentarios_posts1`
    FOREIGN KEY (`posts_idposts`)
    REFERENCES `posts` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_comentarios_usuarios1`
    FOREIGN KEY (`usuarios_idusuarios`)
    REFERENCES `users` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `likes`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `likes` (
  `id` INT NOT NULL COMMENT '',
  `datacriacao` DATE NOT NULL COMMENT '',
  `posts_idposts` INT NOT NULL COMMENT '',
  `usuarios_idusuarios` INT NOT NULL COMMENT '',
  PRIMARY KEY (`id`)  COMMENT '',
  INDEX `fk_curtidas_posts1_idx` (`posts_idposts` ASC)  COMMENT '',
  INDEX `fk_curtidas_usuarios1_idx` (`usuarios_idusuarios` ASC)  COMMENT '',
  CONSTRAINT `fk_curtidas_posts1`
    FOREIGN KEY (`posts_idposts`)
    REFERENCES `posts` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_curtidas_usuarios1`
    FOREIGN KEY (`usuarios_idusuarios`)
    REFERENCES `users` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `logs`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `logs` (
  `id` INT NOT NULL COMMENT '',
  `log` VARCHAR(45) NOT NULL COMMENT '',
  `datacriacao` DATE NOT NULL COMMENT '',
  `usuarios_idusuarios` INT NOT NULL COMMENT '',
  PRIMARY KEY (`id`)  COMMENT '',
  INDEX `fk_logs_usuarios1_idx` (`usuarios_idusuarios` ASC)  COMMENT '',
  CONSTRAINT `fk_logs_usuarios1`
    FOREIGN KEY (`usuarios_idusuarios`)
    REFERENCES `users` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
